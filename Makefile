VERSION=`git describe | sed 's/^v//; s/-/./g' `
CUSTOMER="cud"
NAME="$CUSTOMER-themes"

prefix = /usr

all:
	@(echo "Nothing to build. Please use make install.")

clean:
	rm -rf build sdist

build: clean
	mkdir -p build/$(NAME)-$(VERSION)
	for i in *; do \
		if [ "$$i" != "build" ]; then \
			cp -R "$$i" build/$(NAME)-$(VERSION); \
		fi; \
	done

install:
	mkdir -p $(DESTDIR)$(prefix)/share/authentic2/$(CUSTOMER)
	mkdir -p $(DESTDIR)$(prefix)/share/portail-citoyen2/$(CUSTOMER)
        mkdir -p $(DESTDIR)$(prefix)/share/wcs/themes/$(CUSTOMER)
	cp -r idp/* $(DESTDIR)$(prefix)/share/authentic2/$(CUSTOMER)
	cp -r portail-citoyen/* $(DESTDIR)$(prefix)/share/portail-citoyen2/$(CUSTOMER)
        cp -r wcs/$(CUSTOMER)/* $(DESTDIR)$(prefix)/share/wcs/themes/$(CUSTOMER)

dist-bzip2: build
	mkdir sdist
	cd build && tar cfj ../sdist/$(NAME)-$(VERSION).tar.bz2 .

version:
	@(echo $(VERSION))

name:
	@(echo $(NAME))

fullname:
	@(echo $(NAME)-$(VERSION))

